<div align=center><div><img src="https://res.zrain.fun/uploads/big/4f5523b851f8296534cbfa8af5a86c35.png" alt="echo" style="height:80px" /></div><div style="display:inline-block;font-size:1.8em;font-weight: bold;line-height:80px;margin-left:30px" >Echo | CTF Project</div></div>

<div align=center><img src="https://img.shields.io/badge/action-building-brightgreen" style="margin-right:10px" /><img src="https://img.shields.io/badge/type-example-yellowgreen"/></div>

可能要进行很久的一个项目。。。

#### 启动

启动测试环境：

```bash
npm run dev
```

构建项目：

```bash
npm run build
```

预览（构建 + 测试）：

```bash
npm run serve
```

代码规范性检查：

```bash
npm run lint
```

代码修复：

```bash
npm run fix
```

#### 项目结构

```bash
│  .commitlintrc.json # commitlint配置
│  .cz-config.js # commit消息规范化配置
│  .env.dev # development环境下变量参数
│  .env.prod # production环境变量参数
│  .eslintrc.js # eslint 规则配置（已配置规则详见eslint章节）
│  .gitignore # 懂得都懂
│  .prettierrc.json # perttier配置文件（和eslint做了冲突处理）
│  index.html # 你不需要知道
│  package-lock.json # 你不需要知道
│  package.json # 你不需要知道
│  README.md # 你不需要知道
│  vite.config.js # vite配置文件
│  
├─.husky
│  │  commit-msg # commit hook
│  │  pre-commit # pre commit hook
│  │  
│  └─_
│      .gitignore
│      husky.sh
│          
├─public
│      favicon.ico
│      
└─src
    │  App.vue
    │  config.js
    │  main.js
    │  
    ├─api # api模块
    │  │  index.js
    │  │  
    │  └─modules
    │          example.js
    │          
    ├─assets # 静态资源模块
    │      logo.png
    │      
    ├─components # 组件模块
    │  ├─example
    │  │      counter.vue
    │  │      
    │  └─global # 全局组件
    |
    ├─layout # 布局组件
    │      index.vue
    |
    ├─router # 路由模块
    │  │  index.js
    │  │  
    │  └─modules
    │          example.js
    │          
    ├─store # 状态管理模块
    │  │  index.js
    │  │  
    │  └─modules
    │          example.js
    │          
    ├─style # 全局css配置
    │      index.scss
    │      mixin.scss
    │      palette.scss
    │      
    ├─utils # 工具模块
    │      axios.js
    │      
    └─views # 视图模块
            echo.vue
```

#### 启动环境

获取环境参数：`import.meta.env.VITE_MODE`

默认启动环境参数：

- 开发环境：`DEV`
- 生产环境：`PROD`

#### ESLint

使用[airbnb](https://github.com/airbnb/javascript)代码规范，下面列出额外规则：

```js
module.exports = {
  rules: {
     // 禁止参数赋值操作 
    'no-param-reassign': [
      'error',
      {
        props: true,
        // 排除以下参数
        ignorePropertyModificationsFor: ['config', 'response', 'state']
      }
    ],
     // 禁止无关包
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
     // 禁止默认导出要求
    'import/prefer-default-export': 'off'
  }
}

```

#### Prettier

```json
{
  "useTabs": false,
  "tabWidth": 2,
  "printWidth": 100,
  "singleQuote": true,
  "trailingComma": "none",
  "bracketSpacing": true,
  "semi": false
}
```

#### 提交规范

提交本地仓库请使用`git cz`。项目已开启代码检查修复和commit信息检查。
