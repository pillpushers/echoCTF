import Echo from '../../views/echo.vue'

/**
 * @type import('vue-router').RouteRecordRaw
 */
const example = {
  path: '/',
  component: Echo,
  name: 'echoCTF-example',
  children: []
}

export default [example]
