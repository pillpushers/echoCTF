import { createRouter, createWebHistory } from 'vue-router'

// ROUTERS
import exampleRouters from './modules/example'

const router = createRouter({
  history: createWebHistory(),
  routes: [...exampleRouters]
})

export default router
