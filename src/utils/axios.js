import axios from 'axios'
import { API_BASE_URL } from '../config'

/**
 * @type {import('axios').AxiosInstance}
 */
const Axios = axios.create({
  baseURL: API_BASE_URL,
  timeout: 10000
})

// 当前所有请求
const currentReq = new Map()

/**
 * 添加当前请求
 * @param {import('axios').AxiosRequestConfig} config
 */
function addReq(config) {
  const reqToken = [
    config.method,
    config.url,
    JSON.stringify(config.params),
    JSON.stringify(config.data)
  ].join('&')
  config.cancelToken =
    config.cancelToken ||
    new axios.CancelToken((cancel) => {
      if (!currentReq.has(reqToken)) currentReq.set(reqToken, cancel)
    })
}

/**
 * 删除当前重复的请求
 * @param {import('axios').AxiosRequestConfig} config
 */
function delReq(config) {
  const reqToken = [
    config.method,
    config.url,
    JSON.stringify(config.params),
    JSON.stringify(config.data)
  ].join('&')
  if (currentReq.has(reqToken)) {
    const cancel = currentReq.get(reqToken)
    cancel(reqToken)
    currentReq.delete(reqToken)
  }
}

Axios.interceptors.request.use(
  (config) => {
    delReq(config)
    addReq(config)
    // const token = localStorage.getItem('token')
    // if (token) {
    //   config.headers['Authorization'] = token
    // }
    return config
  },
  (err) => Promise.reject(err)
)

Axios.interceptors.response.use(
  (response) => {
    delReq(response)
    // if (response.status !== 200) {
    //     throw new Error(response.statusText)
    // }
    return response
  },
  (err) => Promise.reject(err)
)

/**
 * 清空请求
 */
export function cleanReq() {
  currentReq.forEach((cancel, url) => cancel(url))
  currentReq.clear()
}

/**
 * 请求构造工厂
 * @param {import('axios').AxiosRequestConfig} config
 * @returns
 */
export const ARFactory = async (config) => Axios(config)
