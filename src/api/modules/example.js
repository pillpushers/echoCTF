import { ARFactory } from '../../utils/axios'

// 示例api
export function exampleAPI(data) {
  return ARFactory({
    url: '/example',
    method: 'GET',
    data
  })
}
