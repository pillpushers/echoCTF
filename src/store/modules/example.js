import { ref } from 'vue'

export default function () {
  // states
  const count = ref(0)
  // mutations
  const reCount = (val) => {
    count.value += val
  }
  // actions

  return {
    state: { count },
    mutations: { reCount },
    actions: {}
  }
}
