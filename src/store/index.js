import useExample from './modules/example'

export const coreState = (() => ({
  example: useExample()
}))()

export const coreStateKey = Symbol('coreState')
