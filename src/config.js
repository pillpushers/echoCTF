export const API_BASE_URL =
  import.meta.env.VITE_MODE === 'DEV'
    ? 'http://dev.example.com/api/'
    : 'http://prod.example.com/api/'
